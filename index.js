var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);


var users = [];
var privateChats=[];
function existeUsuario(user){
	
	for(var i = 0; i < user.length; i++){
		if(users[i] == user){
			return false;
		}
	}

	users.push(user);
	return true;
}

function borrarUsuario(user){
	for(var i = 0; i < users.length; i++){
		if(users[i] == user){
			users.splice(i,1);
		}
	}
}

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  	console.log('a user connected');
  	socket.on('chat message', function(msg, user){
   		console.log(user + ': ' + msg);
   		io.emit('chat message', msg, user);
  	});

  	socket.on('log in', function(msg){
		if(existeUsuario(msg)){
			io.emit('log accepted', msg);
			console.log(msg + ' has logged in');
		}else{
			io.emit('log rejected', msg);
		}
		io.emit('online users', users)
  	});

  	socket.on('bye', function(user){
		console.log(user + ' has disconnected');
		io.emit('bye', user);
		borrarUsuario(user);
		io.emit('online users', users)
  	});

  	socket.on('typing', function(user){
		io.emit('typing', user);
  	});
	
	socket.on('untyping', function(user){
		io.emit('untyping', user);
  	});
	
	socket.on('privPet', function(user){
		io.emit('privPet', user);
  	});

  	socket.on('private bind', function(user, myself){
		if(privateChats[user] == undefined && privateChats[myself] == undefined){
			privateChats[user] = myself;
			privateChats[myself] = user;
			io.emit('private bind', user, myself);
		}
  	});

  	socket.on('private chat', function(msg, user){
		io.emit('private chat', msg, user);
  	});
});

http.listen(3000, function(){
  	console.log('listening on *:3000');
});
